#Compact Component Library: CoCo#

A C++ framework for high-performance multi-thread shared memory applications.
Split your application in component that can run in parallel to each other or be encapsulated one in the other.
Components can run periodically or be triggered by events. 
CoCo provides also a port system allowing the components to communicate with very low overhead without worrying of concurrency issues.

It has been widely used in robotics applications, ranging from Augmented Reality for teleoperation to haptic-rendering for virtual training. 

It is also compatible with ROS allowing it to be used in any kind of high-level control applications.

##Usage##

The documentation for installing and using CoCo can be found on [readthedocs](http://coco.readthedocs.io/en/latest/)

If you have any problem understanding the documentation or you think the docs is not clear please write at filippobrizzi at gmail.com
or use the issue system of github.

##Contribution##

We are always open to people willing to help! If you found any bug or want to propose some new feature or improvement use the github issue system or write at filippobrizzi at gmail.com


##Authors##

This framework has been developed in the Percro Laboratory of the Scuola Superiore Sant'Anna, Pisa by
Filippo Brizzi and Emanuele Ruffaldi.
A special thanks to Nicola Giordani for the web viewer and the review of the code.